import Stopwatch from "./components/stopwatch/stopwatch.component";

function App() {
  return (
    <div className="App">
      <Stopwatch />
    </div>
  );
}

export default App;
