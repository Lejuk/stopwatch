import React, { useState, useEffect } from "react";
import "./stopwatch.styles.css";
import { interval } from "rxjs";
import { takeWhile, takeUntil, scan, filter, map} from "rxjs/operators";
import CustomButton from "../custom-button/custom-button.component";

export default function Stopwatch() {

  const countdown$ = interval(1000);
  
  const [initialCount, setCount] = useState(0);
  const [diff, setDiff] = useState(0);
  const [prevent, setPrevent] = useState(true);
  const [subscription, setSubscription] = useState("");

  // useEffect(() => {
  //   if (isCounting) {
  //     const sub = countdown$.subscribe(setCount);
  //     return () => sub.unsubscribe();
  //   }
  // }, [isCounting]);

  const startOrStopStopwatch = () => {
    if (!subscription) {
      const timerSubscription = countdown$
        .pipe(map((count) => count + 1))
        .subscribe((count) => {
          setCount(count + diff);
        });
      setSubscription(timerSubscription);
    } else {
      subscription.unsubscribe();
      setCount(0);
      setDiff(0);
      setSubscription("");
    }
  };

  const pauseStopWatch = () => {
    if (prevent) {
      setPrevent(false);
      const timerInstance = setTimeout(function () {
        setPrevent(true);
        clearTimeout(timerInstance);
      }, 300);
    } else {
      if (subscription) {
        subscription.unsubscribe();
      }

      setDiff(initialCount);
      setSubscription("");
    }
  };
  
  const resetStopwatch = () => {
    if (subscription) {
      subscription.unsubscribe();
      // setIsCounting(false);
    }
    const timerSubscription = countdown$.subscribe((countet) => {
      setCount(countet);
    });
    setSubscription(timerSubscription);
  };


  const seconds = initialCount % 60;
  const mins = Math.floor(initialCount / 60) % 60;
  const hours = Math.floor(initialCount / 3600);

  const timeFormat = (time) => {
    return time < 10 ? `0${time}` : time;
  };

  return (
    <div className="stopwatch-container">
      <h1 className="stopwatch-container__value">{`${timeFormat(
        hours
      )} : ${timeFormat(mins)} : ${timeFormat(seconds)} `}</h1>
      <div className="stopwatch-container__button-panel">
        <CustomButton action={startOrStopStopwatch}>Start/Stop</CustomButton>
        <CustomButton action={pauseStopWatch}>Pause</CustomButton>
        <CustomButton action={resetStopwatch}>Reset</CustomButton>
      </div>
    </div>
  );
}
