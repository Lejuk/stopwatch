import React from 'react';
import './custom-button.styles.css';

export default function CustomButton({children, action}) {
    return (
        <button className="custom-button" onClick={action}>
            {children}
        </button>
    )
}
